#!/usr/bin/env bash

if [ $# -ne 3 ]
then
	return -1
fi
`eval rm a.out`
`eval mpicc -Wall -Wextra -Werror pingpong.c`
nbpp=4194304
taille=1
echo "nbpp taille temps" > $2
echo "nbpp taille moyenne_temps" > $3
for i in {0..22}
do
	for j in {0..9}
	do
		if [ $1 = "net" ]
		then
			mesures[$j]=`eval mpirun -hostfile hostfile.txt -tune tune.txt -n 2 ./a.out $nbpp $taille`
		elif [ $1 = "loc" ]
                then
			mesures[$j]=`eval mpirun -n 2 ./a.out $nbpp $taille`
		else
			return -1
		fi
		echo "$nbpp $taille ${mesures[$j]}" >> $2
		if [ $j -ne 0 ]
		then
			mesures[0]=`echo "scale=10 ; ${mesures[0]} +  ${mesures[$j]}" | bc`
		fi
	done
	mesure=`echo "scale=10 ; ${mesures[0]} / 10.0" | bc`
	echo "$nbpp $taille $mesure" >> $3
	taille=$(expr $taille '*' 2)
	nbpp=$(expr $nbpp / 2)
done

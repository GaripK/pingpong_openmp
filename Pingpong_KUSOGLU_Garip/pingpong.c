
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main( int argc, char **argv )
{
  MPI_Init(&argc,&argv);

  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int nbpp, taille;
  if(argc!=3)
  {
    if(rank==0)
      fprintf(stderr, "Usage: %s <nbpp> <size>!\n", argv[0]);
    MPI_Finalize();
    return(1);
  }
  nbpp = atoi(argv[1]);
  taille = atoi(argv[2]);


  if( size != 2 || nbpp<=0 || taille<=0 )
  {
    if(rank==0)
      fprintf(stderr, "need two processes and valid parameters!\n");
    MPI_Finalize();
    return(1);
  }

  char *p=malloc(taille);

  double t1 = MPI_Wtime();
  MPI_Request r;
  for( int i=0 ; i<nbpp ; i++ )
  {
    MPI_Issend( p, taille, MPI_CHAR, !rank, 0, MPI_COMM_WORLD, &r);
    MPI_Recv( p, taille, MPI_CHAR, !rank, 0, MPI_COMM_WORLD, NULL );
    MPI_Wait(&r,MPI_STATUS_IGNORE);
  }

  double t2 = MPI_Wtime();
  if(rank==0)
  {
    printf("%lg\n", t2-t1);
  }

  MPI_Finalize();

  return( 0 );
}
